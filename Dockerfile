FROM registry.gitlab.com/etke.cc/base AS builder

WORKDIR /room-purger
COPY . .
RUN make build

FROM alpine:latest

RUN apk --no-cache add ca-certificates tzdata && update-ca-certificates && \
    adduser -D -g '' room-purger

COPY --from=builder /room-purger/room-purger /bin/room-purger

USER room-purger

ENTRYPOINT ["/bin/room-purger"]
