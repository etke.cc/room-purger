package matrix

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
)

const RoomsLimit = 100

// Client implementation
type Client struct {
	homeserver string
	password   string
	login      string
	token      string
}

type loginRequest struct {
	Type       string                 `json:"type"`
	Identifier loginRequestIdentifier `json:"identifier"`
	Password   string                 `json:"password"`
}

type loginRequestIdentifier struct {
	Type string `json:"type"`
	User string `json:"user"`
}

type loginResponse struct {
	AccessToken string `json:"access_token"`
}

type roomsResponse struct {
	Rooms  []roomResponse `json:"rooms"`
	Offset int            `json:"offset"`
	Total  int            `json:"total_rooms"`
}

type roomResponse struct {
	RoomID string `json:"room_id"`
}

type mediaResponse struct {
	Local []string
}

type purgeRequest struct {
	Purge bool `json:"purge"`
}

// New matrix client
func New(homeserver, login, password string) *Client {
	return &Client{
		homeserver: homeserver,
		password:   password,
		login:      login,
	}
}

// Login as matrix user
func (c *Client) Login() error {
	endpoint := c.homeserver + "/_matrix/client/r0/login"
	request, err := json.Marshal(&loginRequest{
		Type: "m.login.password",
		Identifier: loginRequestIdentifier{
			Type: "m.id.user",
			User: c.login,
		},
		Password: c.password,
	})
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", endpoint, bytes.NewReader(request))
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	response := &loginResponse{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return err
	}

	c.token = response.AccessToken

	return nil
}

// nolint // nobody cares about error here, worst case - the session will not be destroyed
func (c *Client) Logout() {
	endpoint := fmt.Sprintf("%s/_matrix/client/r0/logout?access_token=%s", c.homeserver, c.token)
	req, _ := http.NewRequest("POST", endpoint, nil)
	http.DefaultClient.Do(req)
}

// GetRooms from List Rooms Admin API
func (c *Client) GetRooms(search string, offset int) ([]string, error) {
	rooms := []string{}
	endpoint := fmt.Sprintf("%s/_synapse/admin/v1/rooms?access_token=%s&search_term=%s&from=%d", c.homeserver, c.token, url.QueryEscape(search), offset)
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		return rooms, err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return rooms, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return rooms, err
	}
	if resp.StatusCode != 200 {
		return rooms, fmt.Errorf("could not list rooms: %s", string(body))
	}

	var data roomsResponse
	err = json.Unmarshal(body, &data)
	if err != nil {
		return rooms, err
	}

	for _, room := range data.Rooms {
		rooms = append(rooms, room.RoomID)
	}

	var more []string
	if data.Total > offset {
		more, err = c.GetRooms(search, offset+RoomsLimit)
		rooms = append(rooms, more...)
	}

	return rooms, err
}

// GetLocalMedia loads all media from rooms by roomIDs and returns local media IDs
func (c *Client) GetLocalMedia(roomIDs []string) ([]string, error) {
	medias := []string{}
	for _, roomID := range roomIDs {
		log.Println("loading media from", roomID)
		endpoint := fmt.Sprintf("%s/_synapse/admin/v1/room/%s/media?access_token=%s", c.homeserver, roomID, c.token)
		resp, err := http.DefaultClient.Get(endpoint)
		if err != nil {
			return medias, err
		}
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return medias, err
		}

		var data mediaResponse
		err = json.Unmarshal(body, &data)
		if err != nil {
			return medias, err
		}

		for _, item := range data.Local {
			mediaID := strings.ReplaceAll(item, "mxc://", "")
			log.Println("found media", mediaID)
			medias = append(medias, mediaID)
		}
	}

	return medias, nil
}

// PurgeMedia from homeserver
func (c *Client) PurgeMedia(mediaIDs []string) error {
	for _, id := range mediaIDs {
		log.Println("Removing media", id)
		endpoint := fmt.Sprintf("%s/_synapse/admin/v1/media/%s?access_token=%s", c.homeserver, id, c.token)
		req, err := http.NewRequest("DELETE", endpoint, nil)
		if err != nil {
			return err
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return err
		}
		if resp.StatusCode != 200 {
			return fmt.Errorf("could not remove media %s", id)
		}
	}
	return nil
}

// PurgeRooms from homeserver
func (c *Client) PurgeRooms(roomIDs []string) error {
	body, err := json.Marshal(&purgeRequest{Purge: true})
	if err != nil {
		return err
	}

	for _, id := range roomIDs {
		log.Println("Removing room", id)
		endpoint := fmt.Sprintf("%s/_synapse/admin/v2/rooms/%s?access_token=%s", c.homeserver, id, c.token)
		req, err := http.NewRequest("DELETE", endpoint, bytes.NewReader(body))
		if err != nil {
			return err
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return err
		}
		if resp.StatusCode != 200 {
			return fmt.Errorf("could not remove room %s", id)
		}
	}
	return nil
}
