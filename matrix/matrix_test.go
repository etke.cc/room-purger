package matrix

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	expected := &Client{
		homeserver: "https://matrix.example.com",
		login:      "test",
		password:   "test",
	}

	actual := New("https://matrix.example.com", "test", "test")

	if !reflect.DeepEqual(expected, actual) {
		t.Fail()
	}
}

func TestLogin(t *testing.T) {
	expectedRequest := `{"type":"m.login.password","identifier":{"type":"m.id.user","user":"test"},"password":"test"}`
	response := `{"access_token": "token","device_id": "deviceID","user_id": "@test:example.com"}`
	client, server := startServer(t, "/_matrix/client/r0/login", []byte(expectedRequest), []byte(response))
	defer server.Close()

	err := client.Login()
	if err != nil {
		t.Error(err)
	}
	if client.token != "token" {
		t.Error("incorrect token is set")
	}
}

func TestLogout(t *testing.T) {
	client, server := startServer(t, "/_matrix/client/r0/logout?access_token=test", nil, nil)
	client.token = "test"
	defer server.Close()

	client.Logout()
}

func startServer(t *testing.T, expectedPath string, expectedRequestBody []byte, responseBody []byte) (*Client, *httptest.Server) {
	t.Helper()
	client := &Client{
		homeserver: "https://matrix.example.com",
		login:      "test",
		password:   "test",
	}

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		actualPath := r.URL.RequestURI()
		if actualPath != expectedPath {
			t.Error("request url is not expected. actual:", actualPath)
		}
		requestBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			t.Error("sent request body cannot be read")
		}
		if !bytes.Equal(expectedRequestBody, requestBody) {
			t.Log(string(expectedRequestBody), string(requestBody))
			t.Error("sent request body is not expected")
		}
		defer r.Body.Close()

		w.Write(responseBody)
	}))
	client.homeserver = server.URL

	return client, server
}
