# room-purger

Purge rooms using synapse admin api

Consider this project a "bash-oneliner" to do some stuff fast. It's not supposed to be beautiful and shiny, it just must work.

## Features

* Purge matrix rooms (and media inside them) by search criteria, using [List Rooms API](https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#list-room-api) with [Delete Room API v2](https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#version-2-new-version)

## Quick start

```bash
# get the room-purger app first, from the "How to get" section

# room-purger configuration, check the "Configuration" section below to get the full (pretty impressive) list of available options
export RP_HOMESERVER="https://matrix.example.com"
export RP_LOGIN="homeserver_admin_login"
export RP_PASSWORD="homeserver_admin_password"
export RP_SEARCH="search criteria"

room-purger
```
