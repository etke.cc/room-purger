package main

import (
	"log"

	"gitlab.com/etke.cc/room-purger/config"
	"gitlab.com/etke.cc/room-purger/matrix"
)

func main() {
	cfg := config.New()
	log.Println("authorizing...")
	client := matrix.New(cfg.Homeserver, cfg.Login, cfg.Password)
	if err := client.Login(); err != nil {
		panic(err)
	}
	defer client.Logout()
	log.Println("searching rooms...")
	rooms, err := client.GetRooms(cfg.Search, 0)
	if err != nil {
		panic(err)
	}

	log.Println("searching media...")
	media, err := client.GetLocalMedia(rooms)
	if err != nil {
		panic(err)
	}

	log.Println("purging media...")
	err = client.PurgeMedia(media)
	if err != nil {
		panic(err)
	}

	log.Println("purging rooms...")
	err = client.PurgeRooms(rooms)
	if err != nil {
		panic(err)
	}

	log.Println("done")
}
