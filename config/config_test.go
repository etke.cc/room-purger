package config

import (
	"testing"
)

var testenv = map[string]string{
	"RP_HOMESERVER": "https://matrix.example.com",
	"RP_LOGIN":      "test",
	"RP_PASSWORD":   "password",
	"RP_SEARCH":     "WhatsApp Status Broadcast",
}

func TestNew(t *testing.T) {
	for key, value := range testenv {
		t.Setenv(key, value)
	}

	cfg := New()

	if cfg.Homeserver != "https://matrix.example.com" {
		t.Fail()
	}
	if cfg.Login != "test" {
		t.Fail()
	}
	if cfg.Password != "password" {
		t.Fail()
	}
}
