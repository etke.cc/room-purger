package config

import (
	"gitlab.com/etke.cc/go/env"
)

// Config ...
type Config struct {
	// Login is a matrix user login, to use with password
	Login string
	// Password is a matrix user password, to use with login
	Password string
	// Homeserver is a target HS url (delegation not supported)
	Homeserver string
	// Search param
	Search string
}

const prefix = "rp"

// New config
func New() *Config {
	env.SetPrefix(prefix)
	return &Config{
		// connection
		Homeserver: env.String("homeserver", ""),
		Password:   env.String("password", ""),
		Search:     env.String("search", ""),
		Login:      env.String("login", ""),
	}
}
